from flask import Flask, request, jsonify, Response, render_template
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
import datetime
import re
import smtplib
from email.mime.text import MIMEText
from smtplib import SMTPResponseException
import socket

db_connect = create_engine('sqlite:///eventDatabase.db')
app = Flask(__name__)
api = Api(app)


class Events(Resource):
    def get(self):
        try:
            conn = db_connect.connect() 
            filterName = request.args.get('filterName')
            if filterName == None:
                query = conn.execute("select * from Event")
            else:
                query = conn.execute("select * from Event where EventName like '%" +filterName + "%'")
                print("select EventId, Eventname from Event where EventName like '%" + filterName + "%'")
            return {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        finally:
            conn.close()

    def post(self):
        try:
            conn = db_connect.connect()
            auth = request.headers.get("X-Api-Key")
            authdata = conn.execute("select Encpass from Authdata WHERE User='admin'")
            hashpass =  [i[0] for i in authdata.cursor.fetchall()][0]
            if not auth == hashpass:
                return {"message": "ERROR: Unauthorized"}, 401
            EventName = request.json['EventName']
            EventLocation = request.json['EventLocation']
            City = request.json['City']
            State = request.json['State']
            Country = request.json['Country']
            PostalCode = request.json['PostalCode']
            StartTime = request.json['StartTime']
            EndTime = request.json['EndTime']
            EventContactPersonName = request.json['EventContactPersonName']
            EventContactPhone = request.json['EventContactPhone']
            EventContactEmail = request.json['EventContactEmail']
            if not (validation.dateValididate(validation,StartTime)):
                return {'status':'Verification Failed', 'reason': 'Invalid Start Date Time, date time format should be YYYY-MM-DD HH:MM:SS'}, 400
            if not (validation.dateValididate(validation,EndTime)):
                return {'status':'Verification Failed', 'reason': 'Invalid End Date, data time format should be YYYY-MM-DD HH:MM:SS'}, 400
            if not (validation.zipcodeValididate(validation,PostalCode)):
                return {'status':'Verification Failed', 'reason': 'Invalid Postal Code'}, 400
            if not (validation.phoneValididate(validation,EventContactPhone)):
                return {'status':'Verification Failed', 'reason': 'Invalid Phone, please follow following 10 digit phone formats 0123456789 or 012-345-6789 or 012.345.6789'}, 400
            if not (validation.emailValididate(validation,EventContactEmail)):
                return {'status':'Verification Failed', 'reason': 'Invalid Email'}, 400
            query = conn.execute("INSERT INTO Event (EventName, EventLocation, City, State, Country, PostalCode, StartTime, EndTime, EventContactPersonName,EventContactPhone, EventContactEmail) VALUES ('{0}','{1}','{2}','{3}', '{4}','{5}','{6}','{7}','{8}','{9}','{10}')".format(EventName, EventLocation, City, State, Country, PostalCode, StartTime, EndTime, EventContactPersonName, EventContactPhone, EventContactEmail))
            print(query)
            return {'status':'success'}
        finally:
            conn.close()

    def delete(self):
        try:
            conn = db_connect.connect()
            auth = request.headers.get("X-Api-Key")
            authdata = conn.execute("select Encpass from Authdata WHERE User='admin'")
            hashpass =  [i[0] for i in authdata.cursor.fetchall()][0]
            if not auth == hashpass:
                return {"message": "ERROR: Unauthorized"}, 401
            if "EventId" in request.json:
                EventId = request.json['EventId']
            else:
                return {'status':'Failure', 'reason': 'EventId is mandatory field'}, 400
            EventDetail = EventDetails.get(EventDetails, EventId)
            if not EventDetail["data"]:
                return {'status':'Failure', 'reason': 'Event with id:' + str(EventId) + ' does not exist'}, 400
            deleteEvent = conn.execute("DELETE from Event where EventId= '{0}'".format(EventDetail["data"][0]["EventId"]))
            return {'status':'success'}
        finally:
            conn.close()


class EventDetails(Resource):
    def get(self, event_id):
        try:
            conn = db_connect.connect()
            query = conn.execute("select * from Event where EventId='%s'"  %event_id)
            result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
            return result
        finally:
            conn.close()


class RegisterEvent(Resource):
    def post(self, event_id):
        try:
            conn = db_connect.connect()
            EventList = EventDetails.get(EventDetails, event_id)
            if not (EventList["data"]):
                return {'status':'Failure', 'reason': 'Event Does not exist'}, 400
            if "Email" in request.json:
                UserContactEmail = request.json['Email']
            else:
                return {'status':'Failure', 'reason': 'Email is a mandatory fild'}, 400
            if "FirstName" in request.json:
                UserFirstName = request.json['FirstName']
            else:
                UserFirstName = ""
            if "LastName" in request.json:
                UserLastName = request.json['LastName']
            else:
                UserLastName = ""
            if "ContactPhone" in request.json:
                UserContactPhone = request.json['ContactPhone']
            else:
                UserContactPhone = ""
            if not (validation.emailValididate(validation,UserContactEmail)):
                return {'status':'Failed', 'reason': 'Invalid Email'}, 400
            UserDetail = conn.execute("select * from User where UserContactEmail='%s'"  %UserContactEmail)
            UserResult = {'data': [dict(zip(tuple (UserDetail.keys()) ,i)) for i in UserDetail.cursor]}
            if not UserResult["data"]:
                userAdd = conn.execute("INSERT INTO User (UserContactEmail, UserFirstName, UserLastName, UserContactPhone) VALUES ('{0}','{1}','{2}','{3}')".format(UserContactEmail, UserFirstName, UserLastName, UserContactPhone))
                print(userAdd)
                UserDetail = conn.execute("select * from User where UserContactEmail='%s'"  %UserContactEmail)
                UserResult = {'data': [dict(zip(tuple (UserDetail.keys()) ,i)) for i in UserDetail.cursor]}
            print(UserResult["data"][0]["UserId"])
            RegistrationDetail = conn.execute("select * from Registrations where UserId='%s' and EventId='%s'"  %(UserResult["data"][0]["UserId"],EventList["data"][0]["EventId"]))
            RegisterResult = {'data': [dict(zip(tuple (RegistrationDetail.keys()) ,i)) for i in RegistrationDetail.cursor]}
            if not RegisterResult["data"]:
                registrationAdd = conn.execute("INSERT INTO Registrations (UserId, EventId) VALUES ('{0}','{1}')".format(UserResult["data"][0]["UserId"], EventList["data"][0]["EventId"]))
                print(registrationAdd)
            else:
                return {'status':'Already Registered', 'reason': 'The email id ' + UserContactEmail + ' is already registered for event: ' + EventList["data"][0]["EventName"]}, 402
            Helpers.sendEmail(Helpers, "eventorg@events.com", EventList["data"][0]["EventContactEmail"], UserContactEmail + " is registered for " + EventList["data"][0]["EventName"])
            return {'status':'success'}
        finally:
            conn.close()


class UnregisterEvent(Resource):
    def post(self, event_id):
        try:
            conn = db_connect.connect()
            EventList = EventDetails.get(EventDetails, event_id)
            if not (EventList["data"]):
                return {'status':'Failure', 'reason': 'Event Does not exist'}, 400
            if "Email" in request.json:
                UserContactEmail = request.json['Email']
            else:
                return {'status':'Failure', 'reason': 'Email is a mandatory fild'}, 400
            if not (validation.emailValididate(validation,UserContactEmail)):
                return {'status':'Failure', 'reason': 'Invalid Email'}, 400
            UserDetail = conn.execute("select * from User where UserContactEmail='%s'"  %UserContactEmail)
            UserResult = {'data': [dict(zip(tuple (UserDetail.keys()) ,i)) for i in UserDetail.cursor]}
            if not UserResult["data"]:
                return {'status':'Failure', 'reason': 'User Email is not registered for any event'}, 404
            RegistrationDetail = conn.execute("select * from Registrations where UserId='%s' and EventId='%s'"  %(UserResult["data"][0]["UserId"],EventList["data"][0]["EventId"]))
            RegisterResult = {'data': [dict(zip(tuple (RegistrationDetail.keys()) ,i)) for i in RegistrationDetail.cursor]}
            if not RegisterResult["data"]:
                return {'status':'Failure', 'reason': 'The user with email id ' + UserContactEmail + ' is not regidtered for the event: ' + EventList["data"][0]["EventName"]}, 404
            else:
                registrationRemove = conn.execute("Delete from Registrations where RegistrationId='{0}'".format(RegisterResult["data"][0]["RegistrationId"]))
                print(registrationRemove.cursor)
            return {'status':'success'}
        finally:
            conn.close()


class GetAllUserForEvent(Resource):
    def get(self, event_id):
        try:
            conn = db_connect.connect()
            auth = request.headers.get("X-Api-Key")
            authdata = conn.execute("select Encpass from Authdata WHERE User='admin'")
            hashpass =  [i[0] for i in authdata.cursor.fetchall()][0]
            if not auth == hashpass:
                return {"message": "ERROR: Unauthorized"}, 401
            EventList = EventDetails.get(EventDetails, event_id)
            if not (EventList["data"]):
                return {'status':'Failure', 'reason': 'Event with id:' + event_id + ' does not exist'}, 400
            query = conn.execute("select * from User WHERE UserId IN (select UserId from registrations where EventId=%s)" %(event_id)) 
            return {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        finally:
            conn.close()


class GetAllEventsForUser(Resource):
    def get(self):
        try:
            conn = db_connect.connect()
            userEmail = request.args.get('emailId')
            if userEmail == None:
                return {'status':'Failure', 'reason': 'emailId is mandatory parameter'}, 400
            userIdData = conn.execute("select UserId from user WHERE UserContactEmail='%s'" %userEmail)
            userResult = {'data': dict(zip(tuple (userIdData.keys()) ,i)) for i in userIdData.cursor}
            print(userResult)
            if not userResult:
                return {'status':'Not Found', 'reason': 'User with ' + userEmail + ' is not registered to any event'}, 404
            eventDetails = conn.execute("select EventId from Registrations WHERE UserId='%s'" %userResult["data"]["UserId"])
            eventData = {'data': [dict(zip(tuple (eventDetails.keys()) ,i)) for i in eventDetails.cursor]}
            eventData = tuple(i["EventId"] for i in eventData["data"])
            if len(eventData) == 1:
                eventData = "(" + str(eventData[0]) + ")"
            print(eventData)
            eventQuery = conn.execute("select EventName from event where eventid in %s" %str(eventData))
            return {'data': [dict(zip(tuple (eventQuery.keys()) ,i)) for i in eventQuery.cursor]}
            
        finally:
            conn.close()


class Helpers():
    def sendEmail(self, sender, receivers, message):
        try:
            smtpGatewayAddress = ''
            smtpObj = smtplib.SMTP(smtpGatewayAddress)
            message = MIMEText(message)
            message['Subject'] = 'New user registered'
            smtpObj.sendmail(sender, receivers, message.as_string())
            print("Successfully sent email")
            smtpObj.quit()
        except socket.error as e:
            print(e)
        except SMTPResponseException as e:
            print("Error: unable to send email, " + str(e.smtp_code) + ": " + str(e.smtp_error))


class validation():
    def zipcodeValididate(self, zip):
        regex = '^[0-9]{5}(?:-[0-9]{4})?$'
        if(re.search(regex,zip)):  
            print("Valid zip code")
            return True
        else:  
            print("Invalid zip code")  
            return False

    def phoneValididate(self, phone):
        regex = '^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$'
        if(re.search(regex,phone)):  
            print("Valid Phone")
            return True
        else:  
            print("Invalid Phone")  
            return False

    def emailValididate(self, email):
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        if(re.search(regex,email)):  
            print("Valid Email")
            return True
        else:  
            print("Invalid Email")  
            return False
    
    def dateValididate(self, date_string):
        date_format = '%Y-%m-%d %H:%M:%S'
        try:
            date_obj = datetime.datetime.strptime(date_string, date_format)
            print(date_obj)
            return True
        except ValueError:
            print("Incorrect data format, should be YYYY-MM-DD")
            return False

api.add_resource(Events, '/api/v1/events') 
api.add_resource(EventDetails, '/api/v1/event/<event_id>')
api.add_resource(RegisterEvent, '/api/v1/register/<event_id>')
api.add_resource(UnregisterEvent, '/api/v1/unregister/<event_id>')
api.add_resource(GetAllUserForEvent, '/api/v1/event/list_all_users/<event_id>')
api.add_resource(GetAllEventsForUser, '/api/v1/user/list_events')

@app.route('/')
@app.route('/index')
def index():
    try:
        return render_template("events.html")
    except:
        return render_template("error.html")

@app.route('/event/<event_id>')
def eventDetails(event_id):
    try:
        return render_template("eventDetails.html", value=event_id)
    except:
        return render_template("error.html")

@app.route('/error')
def errorPage():
    try:
        return render_template("error.html")
    except:
        return render_template("error.html")

@app.route('/success')
def successPage():
    try:
        return render_template("success.html")
    except:
        return render_template("error.html")

@app.route('/myregistrations')
def myRegistrations():
    try:
        return render_template("myRegistrations.html")
    except:
        return render_template("error.html")

if __name__ == '__main__':
     app.run(port='5000')